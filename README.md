# Django-Portfolio

Repozytorium zawiera małe portfolio przedstawiające moją osobę. Projekt został wykonany głównie w celu przypomnienia sobie frameworka Django v2.2.6. 
Zdecydowanie bardziej interesuję się częścią back-endową dlatego front-end został zaczerpnięty od Blackrock Digital dostępny pod <https://github.com/BlackrockDigital/startbootstrap-resume>. Kod został odpowiednio zmodyfikowany pod własne wymagania - zgodnie z warunkami licencji.

# Wygląd portfolio
![About](images/1_r.png)
![Projects](images/2_r.png)
![Education](images/3_r.png)
