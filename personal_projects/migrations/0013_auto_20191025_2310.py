# Generated by Django 2.2.6 on 2019-10-25 21:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('personal_projects', '0012_auto_20191025_0056'),
    ]

    operations = [
        migrations.AlterField(
            model_name='project',
            name='finish_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
