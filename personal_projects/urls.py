from django.urls import path, include
from . import views
from django.conf import settings # new
from django.conf.urls.static import static # new

urlpatterns = [
    path('list/', views.ProjectList.as_view(), name='project_list'),
    path('<int:pk>/', views.ProjectDetail.as_view(), name='project_detail'),
]