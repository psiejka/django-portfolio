from django.shortcuts import render
from django.views.generic import ListView, DetailView, TemplateView
from .models import Project

# Create your views here.
class ProjectList(ListView):
    queryset = Project.objects.all()
    context_object_name = 'projects'
    template_name = 'project_list.html'


class ProjectDetail(DetailView):
    model = Project
    template_name = 'project_detail.html'

