from django.shortcuts import render
from django.views.generic import DetailView, TemplateView

class About(TemplateView):
    template_name = 'about.html'


class Education(TemplateView):
    template_name = 'education.html'
